module gitlab.com/micro/client_example

go 1.15

require (
	github.com/gin-gonic/gin v1.7.4
	github.com/golang/protobuf v1.5.2
	github.com/micro/micro/v3 v3.3.0
	google.golang.org/protobuf v1.27.1
)
