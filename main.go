package main

import (
	"context"
	"fmt"
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	proto "gitlab.com/micro/client_example/proto"

	"github.com/micro/micro/v3/service"
)

func main() {
	fmt.Println("Started")

	// create and initialise a new service
	srv := service.New()

	// create the proto client for helloworld
	client := proto.NewHelloworldService("examplemicro", srv.Client())

	// call an endpoint on the service
	r := gin.Default()

	r.GET("/hello/:name/", func(c *gin.Context) {
		name := c.Param("name")
		if name == "" {
			name = "World"
		}
		rsp, err := client.Call(context.Background(), &proto.Request{Name: name})
		if err != nil {
			log.Println(err)
			return
		}
		c.JSON(http.StatusOK, rsp)
	})

	log.Println(r.Run("localhost:7000"))
}
